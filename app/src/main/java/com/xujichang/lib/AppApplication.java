package com.xujichang.lib;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.xujichang.crashlog.CrashHandler;

/**
 * Created by user on 2017/3/29.
 */

public class AppApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        CrashHandler crashHandler = CrashHandler.newInstance();
        crashHandler.init(getApplicationContext(), "http://10.10.100.6:9987/restful/crashlog/", true);
    }
}
