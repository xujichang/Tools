package com.xujichang.lib;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private Button btCrash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    public void onViewClicked() {
        //创建一个BUG
        ArrayList<String> strings = new ArrayList<>();
        strings.get(0);
    }

    private void initView() {
        btCrash = (Button) findViewById(R.id.bt_crash);
        btCrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onViewClicked();
            }
        });
    }
}
