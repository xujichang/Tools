package com.xujichang.crashlog;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by xjc on 2017/6/6.
 */

public interface API {
    @POST
    Observable<Integer> postCrashLog(@Url String url, @Body String log);
}
