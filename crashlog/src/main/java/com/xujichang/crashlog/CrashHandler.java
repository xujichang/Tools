package com.xujichang.crashlog;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Debug;
import android.os.Looper;
import android.os.Process;
import android.text.TextUtils;
import android.widget.Toast;

import com.xujichang.crashlog.bean.Crash;

import org.json.JSONException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

/**
 * Created by user on 2017/3/29.
 */


public class CrashHandler implements Thread.UncaughtExceptionHandler {

    private static CrashHandler handler = null;
    private Context context;
    private Thread.UncaughtExceptionHandler defaultHandler;
    private Class restartActivity;
    private LogManager logManager;
    private LogUtils logUtils;
    private CrashHandler.RestartListener listener;

    private CrashHandler() {
    }

    public static CrashHandler newInstance() {
        if (null == handler) {
            handler = new CrashHandler();
        }

        return handler;
    }

    public void init(Context context, String url, Class restartActivity, boolean isDeBug) {
        this.context = context.getApplicationContext();
        this.restartActivity = restartActivity;
        Const.CRASH_BASE_URL = url;
        logUtils = LogUtils.getInstance();
        logManager = LogManager.newInstance(context);
        logManager.setIsDeBug(isDeBug);
        logUtils.setIsDeBug(isDeBug);
        defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == connectivityManager.getActiveNetworkInfo()) {
            logUtils.logI("----------------------网络不可用", "CrashHandler");
        } else {
            logManager.checkLog();
        }
    }

    public void init(Context context, String url, boolean isDeBug) {
        init(context, url, (Class) null, isDeBug);
    }

    public void uncaughtException(Thread t, Throwable e) {
        if (!handleException(e) && null != defaultHandler) {
            defaultHandler.uncaughtException(t, e);
        } else {
            new Thread() {
                public void run() {
                    Looper.prepare();
                    Toast.makeText(CrashHandler.this.context, "很抱歉,程序出现异常,即将退出.", Toast.LENGTH_SHORT).show();
                    Looper.loop();
                }
            }.start();

            try {
                Thread.sleep(2000L);
            } catch (InterruptedException ee) {
                ee.printStackTrace();
            }

            restartApp();
        }

    }

    private boolean handleException(Throwable e) {
        logUtils.logI("----------------------------------------处理Exception：", "CrashHandler");
        if (null == e) {
            return false;
        } else {
            try {
                Crash crash = getExceptionInfo(e);
                logManager.saveExceptionLog(crash);
                return true;
            } catch (JSONException ee) {
                ee.printStackTrace();
                return false;
            }
        }
    }

    private void restartApp() {
        logUtils.logI("----------------------------------------重启APP：", "CrashHandler");
        if (null != this.listener) {
            listener.restart();
        }

        if (null != restartActivity) {
            AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, this.restartActivity);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent restartIntent = PendingIntent
                    .getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            mgr.set(1, System.currentTimeMillis() + 500L, restartIntent);
        }

        Process.killProcess(Process.myPid());
        System.exit(0);
        System.gc();
    }

    private Crash getExceptionInfo(Throwable e) throws JSONException {
        Crash crash = new Crash();
        crash.setPackageName(getPackageName());
        crash.setActivityName(getRunningTopActivityName());
        crash.setCrashMsg(getCrashMsg(e));
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        e.printStackTrace(printWriter);
        Throwable cause = e.getCause();
        while (cause != null) {
            cause.printStackTrace(printWriter);
            cause = cause.getCause();
        }
        printWriter.close();
        String result = writer.toString();
        crash.setStackInfo(result);
        crash.setDeviceInfo(DeviceUtils.newInstance().getDeviceInfo(context));

        logUtils.logI("----------------------------------------获取错误信息：" + crash.toString(),
                "CrashHandler");
        return crash;
    }

    private String getCrashMsg(Throwable e) {
        String msg = e.getMessage();
        if (TextUtils.isEmpty(msg)) {
            msg = e.getCause().getMessage();
        }
        if (TextUtils.isEmpty(msg)) {
            msg = e.getClass().getSimpleName();
        }
        return msg;
    }

    private String getRunningTopActivityName() {
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        // get the info from the currently running task
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        return componentInfo.getClassName();
    }

    private String getPackageName() {
        String version = "版本未知";
        String packageName = "包名未知";
        try {
            packageName = context.getPackageName();
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(packageName, 0);
            version = info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "packageName:" + packageName + "   version:" + version;
        }
        return "packageName:" + packageName + "   version:" + version;
    }

    public void setOnRestartListener(CrashHandler.RestartListener listener) {
        this.listener = listener;
    }

    public interface RestartListener {

        void restart();
    }

    private void getRunningInfo() {
        StringBuilder builder = new StringBuilder("Activity运行信息:\n");
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryClass();
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        activityManager.getMemoryInfo(memoryInfo);
        builder.append("在低内存下运行：").append(memoryInfo.lowMemory).append("\n");
        builder.append("总内存：").append(memoryInfo.totalMem).append("\n");
        builder.append("可用内存：").append(memoryInfo.availMem).append("\n");

        Debug.MemoryInfo debugInfo = new Debug.MemoryInfo();
        Debug.getMemoryInfo(debugInfo);
        builder.append("在低内存下运行：").append(debugInfo.dalvikPss).append("\n");
        builder.append("在低内存下运行：").append(memoryInfo.threshold).append("\n");
        builder.append("在低内存下运行：").append(memoryInfo.lowMemory).append("\n");
        builder.append("Activity信息：\n");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //6.0
            builder.append("M is for Marshmallow!\n");
            List<ActivityManager.AppTask> appTasks = activityManager.getAppTasks();
            ActivityManager.RecentTaskInfo appTaskInfo = appTasks.get(0).getTaskInfo();
            builder.append("TopActivity：").append(appTaskInfo.topActivity.getClassName()).append("\n");
            builder.append("BaseActivity：").append(appTaskInfo.baseActivity.getClassName()).append("\n");
            builder.append("origActivity：").append(appTaskInfo.origActivity.getClassName()).append("\n");
            builder.append("Activity总数量：").append(appTaskInfo.numActivities).append("\n");
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //5.0
            builder.append("L is for Lollipop!\n");
            ActivityManager.RunningAppProcessInfo info = activityManager.getRunningAppProcesses().get(0);
            builder.append("importanceReasonComponent：").append(info.importanceReasonComponent)
                    .append("\n");
            builder.append("processName：").append(info.processName).append("\n");
            builder.append("pkgList：").append(Arrays.asList(info.pkgList).toString()).append("\n");
        } else {
            //
            builder.append("blow  Lollipop!\n");
            List<ActivityManager.RunningTaskInfo> runningTaskInfoes = activityManager.getRunningTasks(1);
            ActivityManager.RunningTaskInfo runningTaskInfo = runningTaskInfoes.get(0);
            builder.append("TopActivity：").append(runningTaskInfo.topActivity.getClassName())
                    .append("\n");
            builder.append("BaseActivity：").append(runningTaskInfo.baseActivity.getClassName())
                    .append("\n");
            builder.append("Activity总数量：").append(runningTaskInfo.numActivities).append("\n");
            builder.append("正在运行的Activity数：").append(runningTaskInfo.numRunning).append("\n");
        }
        this.logUtils
                .logI("----------------------------------------获取Activity运行信息：" + builder.toString(),
                        "CrashHandler");
    }
}