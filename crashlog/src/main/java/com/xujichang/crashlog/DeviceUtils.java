package com.xujichang.crashlog;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;


import com.xujichang.crashlog.bean.DeviceInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.Method;

/**
 * Created by user on 2017/3/29.
 */


public class DeviceUtils {
    private static DeviceUtils deviceUtils = null;
    private static TelephonyManager manager;

    private DeviceUtils() {
    }

    static DeviceUtils newInstance() {
        if (deviceUtils == null) {
            deviceUtils = new DeviceUtils();
        }

        return deviceUtils;
    }

    String getDeviceInfo(Context context, JSONObject info) throws JSONException {
        manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        info.put("deviceid", this.getDeviceId());
        StringBuilder builder = new StringBuilder(this.getNetInfo(context));
        builder.append("\nDevice Info:");
        builder.append("cpu_name:").append(this.getCpuName()).append("   \n设备ID:").append(this.getDeviceId()).append("   \n手机品牌:").append(this.getPhoneBrand()).append("   \n手机型号:").append(this.getPhoneModel()).append("   \nAndroid API等级:").append(this.getBuildLevel()).append("   \nAndroid 版本:").append(this.getBuildVersion()).append("   \n系统版本显示：").append(this.getOsDisplay()).append("   \nHardWare:").append(this.getHardWare()).append("   \nRom厂商:").append(this.getRom()).append("   \nmac地址:").append(this.getMac()).append("   \n设备序列号: ").append(this.getSerialNumber()).append("   \n软件版本:").append(this.getDeviceSoftwareVersion()).append("   \n国际移动用户识别码:").append(this.getSubscriberId()).append("   \n移动运营商:").append(manager.getSimOperatorName()).append("   \nSIM卡识别码:").append(manager.getSimSerialNumber()).append("   \nSIM卡状态:").append(this.getSimState());
        return builder.toString();
    }

    private String getSimState() {
        String state = "未获得状态";
        switch (manager.getSimState()) {
            case 0:
                state = "未获得状态";
                break;
            case 1:
                state = "SIM卡不可用";
                break;
            case 2:
                state = "Locked: requires the user\'s SIM PIN to unlock";
                break;
            case 3:
                state = "Locked: requires the user\'s SIM PUK to unlock ";
                break;
            case 4:
                state = "Locked: requires a network PIN to unlock";
                break;
            case 5:
                state = "正常";
        }

        return state;
    }

    private String getRom() {
        return Build.MANUFACTURER;
    }

    private String getHardWare() {
        return Build.HARDWARE;
    }

    private String getOsDisplay() {
        return Build.DISPLAY;
    }

    private String getCpuName() {
        try {
            FileReader fr = new FileReader("/proc/cpuinfo");
            BufferedReader br = new BufferedReader(fr);
            String text = br.readLine();
            String[] array = text.split(":\\s+", 2);
            return array[1];
        } catch (FileNotFoundException var6) {
            var6.printStackTrace();
        } catch (IOException var7) {
            var7.printStackTrace();
        }

        return null;
    }

    private String getDeviceId() {
        String deviceId = manager.getDeviceId();
        return deviceId == null ? "" : deviceId;
    }

    private String getPhoneBrand() {
        return Build.BRAND;
    }

    private String getPhoneModel() {
        return Build.MODEL;
    }

    private int getBuildLevel() {
        return Build.VERSION.SDK_INT;
    }

    private String getBuildVersion() {
        return Build.VERSION.RELEASE;
    }

    private String getMac() {
        String macSerial = "";
        String str = "";

        try {
            Process pp = Runtime.getRuntime().exec("cat /sys/class/net/wlan0/address ");
            InputStreamReader ir = new InputStreamReader(pp.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);

            while (null != str) {
                str = input.readLine();
                if (str != null) {
                    macSerial = str.trim();
                    break;
                }
            }
        } catch (IOException var6) {
            var6.printStackTrace();
        }

        if (TextUtils.isEmpty(macSerial)) {
            macSerial = "不可用";
        }

        return macSerial;
    }

    private String getSerialNumber() {
        String serial = null;

        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", new Class[]{String.class});
            serial = (String) get.invoke(c, new Object[]{"ro.serialno"});
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return serial;
    }

    private String getDeviceSoftwareVersion() {
        return manager.getDeviceSoftwareVersion();
    }

    private String getSubscriberId() {
        return manager.getSubscriberId();
    }

    private String getNetInfo(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (null == connectivityManager) {
            return null;
        } else {
            StringBuilder builder = new StringBuilder("NetInfo:");
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (null != networkInfo) {
                switch (networkInfo.getType()) {
                    case 0:
                        builder.append("上网方式：移动网络(").append(networkInfo.getSubtypeName()).append(")");
                        break;
                    case 1:
                        builder.append("上网方式：WIFI");
                        WifiManager manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                        builder.append("(状态：" + this.transWifiState(manager.getWifiState()));
                        WifiInfo wifiInfo = manager.getConnectionInfo();
                        builder.append(" SSID:").append(wifiInfo.getSSID()).append(")");
                        break;
                    case 7:
                        builder.append("上网方式：BlueTooth");
                        networkInfo.getSubtypeName();
                }
            } else {
                builder.append("未连接互联网");
            }

            return builder.toString();
        }
    }

    private String transWifiState(int wifiState) {
        String state = "unknown";
        switch (wifiState) {
            case 0:
                state = "disabling";
                break;
            case 1:
                state = "disabled";
                break;
            case 2:
                state = "enabling";
                break;
            case 3:
                state = "enabled";
                break;
            case 4:
                state = "unknown";
        }

        return state;
    }

    public DeviceInfo getDeviceInfo(Context context) {
        manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        DeviceInfo info = new DeviceInfo();
        //1
        info.setDeviceId(getDeviceId());

        //2
        info.setBuildLevel(getBuildLevel());
        //3
        info.setBuildVersion(getBuildVersion());
        //4
        info.setCpuInfo(getCpuName());
        //5
        info.setDeviceSoftWareVersion(getDeviceSoftwareVersion());
        //6
        info.setHardWare(getHardWare());
        //7
        info.setMac(getMac());
        //8
        info.setNetInfo(getNetInfo(context));
        //9
        info.setOsDisplay(getOsDisplay());
        //10
        info.setPhoneBrand(getPhoneBrand());
        //11
        info.setPhoneModel(getPhoneModel());
        //12
        info.setRom(getRom());
        //13
        info.setSerialNum(getSerialNumber());
        //14
        info.setSimState(getSimState());
        //15
        info.setSimOperatorName(manager.getSimOperatorName());
        //16
        info.setSerialNum(manager.getSimSerialNumber());
        //17
        info.setSubscriberId(getSubscriberId());
        return info;
    }
}