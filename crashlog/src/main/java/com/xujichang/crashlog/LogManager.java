package com.xujichang.crashlog;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.xujichang.crashlog.bean.Crash;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.ResourceObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.http.HTTP;

/**
 * Created by user on 2017/3/29.
 */


public class LogManager {
    private static final String TAG = "CrashHandler";
    private static LogManager manager = null;
    private static SharedPreferences preferences;
    private static String BASE_PATH;
    private static LogUtils logUtils;

    private LogManager() {
    }

    public static LogManager newInstance(Context context) {
        if (null == manager) {
            manager = new LogManager();
        }

        String packageName = context.getPackageName();
        preferences = context.getSharedPreferences(packageName + "_log", 0);
        BASE_PATH = context.getCacheDir().getPath();
        logUtils = LogUtils.getInstance();
        return manager;
    }

    void saveExceptionLog(Crash crash) {
        File file = new File(BASE_PATH, "log_" + System.currentTimeMillis() + ".txt");
        ObjectWriter.write(crash, file);
        logUtils.logI("----------------------------------------保存日志：(name:" + file.getName() + "    path:" + file.getAbsolutePath() + ")", "CrashHandler");
        preferences.edit().putBoolean("isNew", true).putString("path", file.getAbsolutePath()).commit();
    }

    private String getLatestLog(File file) {
        String str = null;
        FileInputStream fls = null;
        ObjectInputStream ois = null;

        try {
            fls = new FileInputStream(file);
            ois = new ObjectInputStream(fls);
            str = (String) ois.readObject();
        } catch (ClassNotFoundException | IOException var14) {
            var14.printStackTrace();
        } finally {
            try {
                if (null != fls) {
                    fls.close();
                }

                if (null != ois) {
                    ois.close();
                }
            } catch (IOException var13) {
                var13.printStackTrace();
            }

        }

        return str;
    }

    private boolean isHasNewLog() {
        return preferences.getBoolean("isNew", false);
    }

    private void sendInfoToServer(Crash crash) {
        RetrofitManager.getInstance()
                .createReq(API.class)
                .postCrashLog(Const.CRASH_BASE_URL, new Gson().toJson(crash))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new ResourceObserver<Integer>() {
                    @Override
                    public void onNext(Integer integer) {
                        if (integer == 201) {
                            preferences.edit().putBoolean("isNew", false).apply();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void sendInfoToServer(String str) {
        HttpURLConnection connection = null;
        PrintWriter writer = null;

        try {
            URL realUrl = new URL(Const.CRASH_BASE_URL);
            connection = (HttpURLConnection) realUrl.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoInput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.connect();
            writer = new PrintWriter(connection.getOutputStream());
            writer.write(str);
            writer.flush();
            int response = connection.getResponseCode();
            if (response == 201) {
                preferences.edit().putBoolean("isNew", false).apply();
                logUtils.logI("----------------------------------------上传服务器：成功 " + str, "CrashHandler");
            } else {
                logUtils.logI("----------------------------------------上传服务器：失败(" + response + ")", "CrashHandler");
            }
        } catch (IOException var10) {
            var10.printStackTrace();
        } finally {
            if (null != connection) {
                connection.disconnect();
            }

            if (null != writer) {
                writer.close();
            }

        }

    }

    void checkLog() {
        logUtils.logI("----------------------------------------检测新日志： " + String.valueOf(this.isHasNewLog()), "CrashHandler");
        if (this.isHasNewLog()) {
            String path = preferences.getString("path", "");
            if (!TextUtils.isEmpty(path)) {
                final File file = new File(path);
                if (file.exists()) {
//                    sendInfoToServer(ObjectWriter.<Crash>read(file));
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Crash crash = ObjectWriter.read(file);
                            sendInfoToServer(new Gson().toJson(crash));
                        }
                    }).start();
                }

            }
        }
    }

    public void setIsDeBug(boolean isDeBug) {
        logUtils.setIsDeBug(true);
    }
}
