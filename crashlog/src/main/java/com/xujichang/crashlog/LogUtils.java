package com.xujichang.crashlog;

import android.util.Log;

/**
 * Created by user on 2017/3/29.
 */


public class LogUtils {
    private static LogUtils instance = null;
    private boolean isDeBug = false;

    private LogUtils() {
    }

    public static LogUtils getInstance() {
        if(null == instance) {
            instance = new LogUtils();
        }

        return instance;
    }

    public void setIsDeBug(boolean isDeBug) {
        this.isDeBug = isDeBug;
    }

    public void logV(String s, String tag) {
        this.transLog(2, tag, s);
    }

    public void logI(String s, String tag) {
        this.transLog(4, tag, s);
    }

    public void logD(String s, String tag) {
        this.transLog(3, tag, s);
    }

    public void logE(String s, String tag) {
        this.transLog(6, tag, s);
    }

    public void logW(String s, String tag) {
        this.transLog(5, tag, s);
    }

    public void logWtf(String s, String tag) {
        this.transLog(7, tag, s);
    }

    private void transLog(int type, String tag, String s) {
        if(this.isDeBug) {
            switch(type) {
                case 2:
                    Log.v(tag, s);
                    break;
                case 3:
                    Log.d(tag, s);
                    break;
                case 4:
                    Log.i(tag, s);
                    break;
                case 5:
                    Log.w(tag, s);
                    break;
                case 6:
                    Log.e(tag, s);
                    break;
                case 7:
                    Log.wtf(tag, s);
            }

        }
    }
}
