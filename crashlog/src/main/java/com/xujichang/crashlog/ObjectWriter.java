package com.xujichang.crashlog;

import android.content.Context;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 对象的持久化操作
 */
public class ObjectWriter {

    /**
     * 写入本地文件
     *
     * @param obj
     * @param file
     */
    public static <T> void write(T obj, File file) {
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            ObjectOutputStream oout = new ObjectOutputStream(bout);
            oout.writeObject(obj);
            oout.flush();
            oout.close();
            bout.close();
            byte[] b = bout.toByteArray();
            FileOutputStream out = new FileOutputStream(file);
            out.write(b);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 从本地文件读取
     *
     * @param file
     * @return
     */
    public static <T> T read(File file) {
        // 拿出持久化数据
        Object obj = null;
        try {
            FileInputStream in = new FileInputStream(file);
            ObjectInputStream oin = new ObjectInputStream(in);
            obj = oin.readObject();
            in.close();
            oin.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) obj;
    }
}
