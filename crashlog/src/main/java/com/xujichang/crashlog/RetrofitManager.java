package com.xujichang.crashlog;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 对Retrofit做进一步封装，请求时 更方便
 * Created by xujichang on 2017/4/13.
 */

public class RetrofitManager {
    private static RetrofitManager ourInstance = null;
    private Retrofit retrofit;

    public static RetrofitManager getInstance() {
        if (null == ourInstance) {
            ourInstance = new RetrofitManager();
        }
        return ourInstance;
    }

    private RetrofitManager() {
        initRetrofit();
    }

    /**
     * 初始化
     */
    private void initRetrofit() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new RespCheckIntercept());
        builder.addInterceptor(loggingInterceptor);//添加日志打印

        builder.connectTimeout(15, TimeUnit.SECONDS);//连接超时 时间
        builder.readTimeout(50, TimeUnit.SECONDS);
        builder.writeTimeout(50, TimeUnit.SECONDS);
        builder.retryOnConnectionFailure(true);//重试

        OkHttpClient client = builder.build();

        retrofit = new Retrofit
                .Builder()
                .baseUrl(Const.CRASH_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())//使用Gson转换
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())//使用RxJava返回格式
                .client(client)
                .build();
    }

    /**
     * 对返回结果进行判断
     */
    private class RespCheckIntercept implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Response response = chain.proceed(chain.request());
            return response;
        }
    }

    public <T> T createReq(Class<T> apiService) {
        return retrofit.create(apiService);
    }
}

