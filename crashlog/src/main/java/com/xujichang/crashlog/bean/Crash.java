package com.xujichang.crashlog.bean;

import java.io.Serializable;

/**
 * Created by xjc on 2017/6/6.
 */

public class Crash implements Serializable {
    private String packageName;
    private String activityName;
    private String crashMsg;
    private DeviceInfo deviceInfo;
    private String stackInfo;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getCrashMsg() {
        return crashMsg;
    }

    public void setCrashMsg(String crashMsg) {
        this.crashMsg = crashMsg;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getStackInfo() {
        return stackInfo;
    }

    public void setStackInfo(String stackInfo) {
        this.stackInfo = stackInfo;
    }

    @Override
    public String toString() {
        return "Crash{" +
                "packageName='" + packageName + '\'' +
                ", activityName='" + activityName + '\'' +
                ", crashMsg='" + crashMsg + '\'' +
                ", deviceInfo=" + deviceInfo +
                ", stackInfo='" + stackInfo + '\'' +
                '}';
    }

}
