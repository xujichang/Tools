package com.xujichang.crashlog.bean;

import java.io.Serializable;

/**
 * builder.append("cpu_name:").append(this.getCpuName())
 * .append("   \n设备ID:").append(this.getDeviceId())
 * .append("   \n手机品牌:").append(this.getPhoneBrand())
 * .append("   \n手机型号:").append(this.getPhoneModel())
 * .append("   \nAndroid API等级:")
 * .append(this.getBuildLevel())
 * .append("   \nAndroid 版本:")
 * .append(this.getBuildVersion())
 * .append("   \n系统版本显示：")
 * .append(this.getOsDisplay())
 * .append("   \nHardWare:")
 * .append(this.getHardWare())
 * .append("   \nRom厂商:")
 * .append(this.getRom())
 * .append("   \nmac地址:")
 * .append(this.getMac())
 * .append("   \n设备序列号: ")
 * .append(this.getSerialNumber())
 * .append("   \n软件版本:")
 * .append(this.getDeviceSoftwareVersion())
 * .append("   \n国际移动用户识别码:")
 * .append(this.getSubscriberId())
 * .append("   \n移动运营商:")
 * .append(manager.getSimOperatorName())
 * .append("   \nSIM卡识别码:")
 * .append(manager.getSimSerialNumber())
 * .append("   \nSIM卡状态:").append(this.getSimState());
 * <p>
 * Created by xjc on 2017/6/6.
 */

public class DeviceInfo implements Serializable {
    private String deviceId;
    private String netInfo;
    private String cpuInfo;
    private String phoneBrand;
    private String phoneModel;
    private int buildLevel;
    private String buildVersion;
    private String osDisplay;
    private String hardWare;
    private String rom;
    private String mac;
    private String serialNum;
    private String deviceSoftWareVersion;
    private String subscriberId;
    private String simOperatorName;
    private String simSerialNumber;
    private String simState;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getNetInfo() {
        return netInfo;
    }

    public void setNetInfo(String netInfo) {
        this.netInfo = netInfo;
    }

    public String getCpuInfo() {
        return cpuInfo;
    }

    public void setCpuInfo(String cpuInfo) {
        this.cpuInfo = cpuInfo;
    }

    public String getPhoneBrand() {
        return phoneBrand;
    }

    public void setPhoneBrand(String phoneBrand) {
        this.phoneBrand = phoneBrand;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public int getBuildLevel() {
        return buildLevel;
    }

    public void setBuildLevel(int buildLevel) {
        this.buildLevel = buildLevel;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        this.buildVersion = buildVersion;
    }

    public String getOsDisplay() {
        return osDisplay;
    }

    public void setOsDisplay(String osDisplay) {
        this.osDisplay = osDisplay;
    }

    public String getHardWare() {
        return hardWare;
    }

    public void setHardWare(String hardWare) {
        this.hardWare = hardWare;
    }

    public String getRom() {
        return rom;
    }

    public void setRom(String rom) {
        this.rom = rom;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    public String getDeviceSoftWareVersion() {
        return deviceSoftWareVersion;
    }

    public void setDeviceSoftWareVersion(String deviceSoftWareVersion) {
        this.deviceSoftWareVersion = deviceSoftWareVersion;
    }

    public String getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    public String getSimOperatorName() {
        return simOperatorName;
    }

    public void setSimOperatorName(String simOperatorName) {
        this.simOperatorName = simOperatorName;
    }

    public String getSimSerialNumber() {
        return simSerialNumber;
    }

    public void setSimSerialNumber(String simSerialNumber) {
        this.simSerialNumber = simSerialNumber;
    }

    public String getSimState() {
        return simState;
    }

    public void setSimState(String simState) {
        this.simState = simState;
    }

    @Override
    public String toString() {
        return "DeviceInfo{" +
                "deviceId='" + deviceId + '\'' +
                ", netInfo='" + netInfo + '\'' +
                ", cpuInfo='" + cpuInfo + '\'' +
                ", phoneBrand='" + phoneBrand + '\'' +
                ", phoneModel='" + phoneModel + '\'' +
                ", buildLevel=" + buildLevel +
                ", buildVersion='" + buildVersion + '\'' +
                ", osDisplay='" + osDisplay + '\'' +
                ", hardWare='" + hardWare + '\'' +
                ", rom='" + rom + '\'' +
                ", mac='" + mac + '\'' +
                ", serialNum='" + serialNum + '\'' +
                ", deviceSoftWareVersion='" + deviceSoftWareVersion + '\'' +
                ", subscriberId='" + subscriberId + '\'' +
                ", simOperatorName='" + simOperatorName + '\'' +
                ", simSerialNumber='" + simSerialNumber + '\'' +
                ", simState='" + simState + '\'' +
                '}';
    }

}
