package me.xujichang.receivebug;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by user on 2017/3/29.
 */

public class LogAdapter extends RecyclerView.Adapter<LogAdapter.LogViewHolder> {

    private ArrayList<String> strings;


    public LogAdapter(ArrayList<String> strings) {
        this.strings = strings;
    }

    @Override
    public LogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_log_adapter, null);
        return new LogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LogViewHolder holder, int position) {
        String string = strings.get(position);
        try {
            holder.bindData(string);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    class LogViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItemLogIndex;
        private TextView tvItemLogDeviceId;
        private TextView tvItemLogTime;
        private TextView tvItemLogContent;

        LogViewHolder(View itemView) {
            super(itemView);
            tvItemLogIndex = (TextView) itemView.findViewById(R.id.tv_item_log_index);
            tvItemLogDeviceId = (TextView) itemView.findViewById(R.id.tv_item_log_device_id);
            tvItemLogTime = (TextView) itemView.findViewById(R.id.tv_item_log_time);
            tvItemLogContent = (TextView) itemView.findViewById(R.id.tv_item_log_content);
        }

        void bindData(String string) throws JSONException {
            JSONObject object = new JSONObject(string);
            tvItemLogIndex.setText(object.getString("id"));
            tvItemLogDeviceId.setText(object.optString("deviceid"));
            tvItemLogTime.setText(getTime(object.optLong("cachetime")));
            tvItemLogContent.setText(object.optString("content"));
        }
    }

    private String getTime(long cachetime) {
        Date date = new Date(cachetime);
        String strs = "";
        DateFormat sdf = new SimpleDateFormat("MM/dd HH:mm:ss", Locale.CHINA);
        strs = sdf.format(date);

        return strs;
    }
}
