package me.xujichang.receivebug;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private static final String url = "http://10.10.100.6:9987/restful/crashlog/ ";
    private LogAdapter adapter;
    private ArrayList<String> stringArrayList;
    private Handler handler;
    HashMap<String, Integer> params;
    private EditText etPage;
    private EditText etSize;
    private SwipeRefreshLayout srfRefresh;
    private RecyclerView rvRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        stringArrayList = new ArrayList<>();
        adapter = new LogAdapter(stringArrayList);
        rvRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvRecycler.setAdapter(adapter);
        handler = new Handler();
        params = new HashMap<>();

        srfRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!TextUtils.isEmpty(etPage.getText()) && !TextUtils.isEmpty(etSize.getText())) {
                    params.clear();
                    params.put(etPage.getText().toString(), 1);
                    params.put(etSize.getText().toString(), 20);
                }
                //刷新
                NetUtil.getInstanc().getLogFromServer(url, params);

            }
        });
        NetUtil.getInstanc().setCallBack(new NetUtil.onLoadCallBack() {
            @Override
            public void onLoadSuccess(final ArrayList<String> strings) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        stringArrayList.clear();
                        stringArrayList.addAll(strings);
                        adapter.notifyDataSetChanged();
                        srfRefresh.setRefreshing(false);
                    }
                });

            }

            @Override
            public void onLoadError(String s) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        srfRefresh.setRefreshing(false);
                    }
                });
            }
        });
//        params.put("loadpage", 1);
//        params.put("pagesize", 20);
        NetUtil.getInstanc().getLogFromServer(url, params);
        srfRefresh.setRefreshing(true);
    }

    private void initView() {
        etPage = (EditText) findViewById(R.id.et_page);
        etSize = (EditText) findViewById(R.id.et_size);
        srfRefresh = (SwipeRefreshLayout) findViewById(R.id.srf_refresh);
        rvRecycler = (RecyclerView) findViewById(R.id.rv_recycler);
    }
}
