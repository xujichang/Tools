package me.xujichang.receivebug;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by user on 2017/3/29.
 */

public class NetUtil {
    private static NetUtil instance;

    private NetUtil() {

    }

    public static NetUtil getInstanc() {
        if (null == instance) {
            instance = new NetUtil();
        }
        return instance;
    }

    public void getLogFromServer(final String url, final HashMap<String, Integer> params) {
        new Thread() {
            @Override
            public void run() {
                ArrayList<String> strings = new ArrayList<>();
                JSONArray array = null;
                try {
                    array = getDataFromServer(url, params);
                    int length = array.length();

                    for (int i = 0; i < length; i++) {
                        strings.add(array.getString(i));
                    }
                    if (null != callBack) {
                        callBack.onLoadSuccess(strings);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }.start();

    }

    private JSONArray getDataFromServer(String url, HashMap<String, Integer> params) throws JSONException {
        HttpURLConnection connection = null;
        InputStream is = null;
        StringBuffer out = null;
        try {
            StringBuilder builder = new StringBuilder(url);

            if (null != params && params.size() > 0) {
                builder.append("?");
                Iterator<Map.Entry<String, Integer>> iterator = params.entrySet().iterator();
                Map.Entry<String, Integer> entry = iterator.next();
                builder.append(entry.getKey()).append("=").append(entry.getValue());
                while (iterator.hasNext()) {
                    Map.Entry<String, Integer> innerEntry = iterator.next();
                    builder.append("&").append(innerEntry.getKey()).append("=").append(innerEntry.getValue());
                }
            }
            URL realUrl = new URL(builder.toString());
            connection = (HttpURLConnection) realUrl.openConnection();
            connection.connect();
            int response = connection.getResponseCode();
            if (response == 200) {
                is = connection.getInputStream();
                out = new StringBuffer();
                byte[] b = new byte[4096];

                int n;
                while ((n = is.read(b)) != -1) {
                    out.append(new String(b, 0, n));
                }
                return new JSONArray(out.toString());
            } else {
                return new JSONArray("[{请求错误：" + response + "}]");
            }
        } catch (IOException var12) {
            var12.printStackTrace();
            return new JSONArray("[{连接失败：}]");
        } finally {
            if (null != connection) {
                connection.disconnect();
            }
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public interface onLoadCallBack {
        void onLoadSuccess(ArrayList<String> strings);

        void onLoadError(String s);
    }

    private onLoadCallBack callBack;

    public void setCallBack(onLoadCallBack callBack) {
        this.callBack = callBack;
    }
}
